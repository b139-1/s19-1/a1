// Check js file
console.log("Hello World!");

// Items 3 & 4
const getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);

// Items 5 & 6
const address = [
	"B30 L8",
	"San Sebastian Street",
	"LJ Ledesma Subdivision",
	"Brgy. Buhang",
	"Jaro",
	"Iloilo City",
	5000
];
const [houseNumber, street, subdivision, barangay, district, city, zipCode] = address;
console.log(`I live in ${houseNumber}, ${street}, ${subdivision}, ${barangay}, ${district}, ${city} ${zipCode}.`);

// Items 7 & 8
const animal = {
	name: "Harambe",
	specie: "western lowland gorilla",
	sName: "gorilla gorilla",
	weight: "440 pounds",
	height: "5 1/2 inches"
}
const {name, sName, specie, weight, height} = animal;
console.log(`${name} is a ${specie}. He weighed ${weight}, and can stand up to ${height}. The scientific name for ${specie} is ${sName}.`);

// Items 9, 10, & 11
let numbers = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20];

numbers.forEach(num => console.log(num));
let sum = numbers.reduce((num1, num2) => num1 + num2);
console.log(sum);

// Items 12 & 13
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const coby = new Dog("Coby", 2, "mixed Shih Tzu/Maltese");
console.log(coby);